using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[CreateAssetMenu(fileName = "Building", menuName = "Building", order = 51)]
public class DataManager : ScriptableObject
{

    [SerializeField] private int capacity_0;
    [SerializeField] private int capacity_1;
    [SerializeField] private int capacity_2;
    [SerializeField] private int currentAmount;
    [SerializeField] private int resourceCount_0;
    [SerializeField] private int resourceCount_1;

    [SerializeField] private GameObject prefResours;

    public int ResourceCount_0
    {
        get { return resourceCount_0; }
        set { resourceCount_0 = value; }
    }
    public int ResourceCount_1
    {
        get { return resourceCount_1; }
        set { resourceCount_1 = value; }
    }
    public GameObject PrefResours
    {
        get { return prefResours; }
    }
    public int Capacity_0
    {
        get { return capacity_0; }
        set { capacity_0 = value; }
    }
    public int Capacity_1
    {
        get { return capacity_1; }
        set { capacity_1 = value; }
    }
    public int Capacity_2
    {
        get { return capacity_2; }
        set { capacity_2 = value; }
    }
    public int CurrentAmount
    {
        get { return currentAmount; }
        set { currentAmount = value; }
    }

}
