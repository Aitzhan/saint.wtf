using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WareHouseID : MonoBehaviour
{
    [SerializeField] private int id;
    
    public int ID
    {
        get { return id; }
    }
}
