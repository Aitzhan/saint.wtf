using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inventory : MonoBehaviour
{
    [SerializeField] private Transform posParent;
    private CreateResource createResource;

    [SerializeField] private float height;
    [SerializeField] private int currentCount;
    [SerializeField] private int id;
    private int maxCount=5;
    
    Stack<GameObject> resources=new Stack<GameObject>();

    private void Start()
    {
        createResource = FindObjectOfType<CreateResource>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "warehouse_0")
        {
            id = other.GetComponent<WareHouseID>().ID;
           InvokeRepeating("AddInventory",.5f,.5f);
        }
        if (other.tag == "warehouse_0_1")
        {
            id = other.GetComponent<WareHouseID>().ID;
            InvokeRepeating("RemoveInventory", .5f, .5f);
        }
        if (other.tag == "warehouse_1")
        {
            id = other.GetComponent<WareHouseID>().ID;
            InvokeRepeating("AddInventory", .5f, .5f);
        }
        if (other.tag == "warehouse_1_1")
        {
            id = other.GetComponent<WareHouseID>().ID;
            InvokeRepeating("RemoveInventory", .5f, .5f);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "warehouse_0")
        {
            CancelInvoke("AddInventory");
        }
        if (other.tag == "warehouse_0_1")
        {
            CancelInvoke("RemoveInventory");
        }
        if (other.tag == "warehouse_1")
        {
            CancelInvoke("AddInventory");
        }
        if (other.tag == "warehouse_1_1")
        {
            CancelInvoke("RemoveInventory");
        }
    }
    void AddInventory()
    {
        if (createResource.data[id].CurrentAmount > 0 && maxCount!=currentCount)
        {
            print(id + " ID Create");
            Transform currentObject = Instantiate(createResource.data[id].PrefResours.transform, new Vector3(posParent.position.x,posParent.position.y+height,posParent.position.z), Quaternion.identity);
            currentObject.SetParent(posParent);
            createResource.data[id].CurrentAmount--;
            height+=.23f;
            currentCount++;
            resources.Push(currentObject.gameObject);
        }
    }
    void RemoveInventory()
    {
     
        if(currentCount > 0)
        {
            if (id==1 && createResource.data[id].ResourceCount_0<10)
            {
                createResource.data[id].ResourceCount_0++;
            }
            if (id == 2) 
            {
               if(createResource.data[id].ResourceCount_0 < 5 && resources.Peek().tag=="resource_0")
                    createResource.data[id].ResourceCount_0++;
               if(createResource.data[id].ResourceCount_1 < 5 && resources.Peek().tag == "resource_1")
                    createResource.data[id].ResourceCount_1++;
            }
        height -= .23f;
        currentCount--;
        Destroy(resources.Peek());
        resources.Pop();
        }
    }
/*    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            for (int i = 0; i < resources.Count; i++)
            {
                Destroy(resources.Peek());
                resources.Pop();
            }
            
        }
    }*/
}
