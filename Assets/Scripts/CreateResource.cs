using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreateResource : MonoBehaviour
{
    public List<DataManager> data;

    [SerializeField] private List<Transform> posBuilding;
    void Start()
    {
        InvokeRepeating("Building_0", 1, 1);
        InvokeRepeating("Building_1", 1, 1);
        InvokeRepeating("Building_2", 1, 1);
    }
    void Building_0()
    {
        if (data[0].Capacity_0 > data[0].CurrentAmount)
        {
           GameObject _resource = Instantiate(data[0].PrefResours, posBuilding[0].position, Quaternion.identity);
            _resource.AddComponent<Rigidbody>();
           _resource.AddComponent<BoxCollider>();
        }
    }
    void Building_1()
    {
        if (data[1].Capacity_1 > data[1].CurrentAmount && data[1].ResourceCount_0 > 0)
        {
            GameObject _resource = Instantiate(data[1].PrefResours, posBuilding[1].position, Quaternion.identity);
            data[1].ResourceCount_0--;
            _resource.AddComponent<Rigidbody>();
            _resource.AddComponent<BoxCollider>();
        }
    }
    void Building_2()
    {
        if (data[2].Capacity_2 > data[2].CurrentAmount && data[2].ResourceCount_0 > 0 && data[2].ResourceCount_1 > 0)
        {
            print("2 create");
            GameObject _resource = Instantiate(data[2].PrefResours, posBuilding[2].position, Quaternion.identity);
            data[2].ResourceCount_0--;
            data[2].ResourceCount_1--;
            _resource.AddComponent<Rigidbody>();
            _resource.AddComponent<BoxCollider>();
        }
    }
}
