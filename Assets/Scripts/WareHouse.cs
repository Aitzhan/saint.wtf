using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WareHouse : MonoBehaviour
{
    private CreateResource createResource;

    [SerializeField] private Text txtAmout;

    public int id;
    void Start()
    {
        createResource = FindObjectOfType<CreateResource>();
        id = GetComponent<WareHouseID>().ID;

        InvokeRepeating("UpdateUI", 1f, 1f);

       
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "resource_0")
        {
            id = 0;
            ImportResouce(0);
            Destroy(other.gameObject);
        }
        if (other.tag == "resource_1")
        {
            if (createResource.data[1].CurrentAmount < 10)
            {
                id = 1;
                ImportResouce(1);
                Destroy(other.gameObject);
            }
        }
        if (other.tag == "resource_2")
        {
            if (createResource.data[2].CurrentAmount < 10)
            {
                id = 2;
                ImportResouce(2);
                Destroy(other.gameObject);
            }
        }

    }
    void ImportResouce(int id)
    {
        createResource.data[id].CurrentAmount++; 
    }
    public void UpdateUI()
    {

        txtAmout.text = $"{createResource.data[id].CurrentAmount}/{createResource.data[id].Capacity_0}";

        if (id == 1 && txtAmout.tag=="resource_0")
        {
            txtAmout.text = $"{createResource.data[id].ResourceCount_0}/{createResource.data[id].Capacity_0}";
        }
        if (id == 1 && txtAmout.tag == "resource_1")
        {
            if (createResource.data[id].ResourceCount_0 == 0)
                txtAmout.text = $"����� ������ 1\n{createResource.data[id].ResourceCount_0}/{createResource.data[id].Capacity_1}";
            else
                txtAmout.text = $"{createResource.data[id].ResourceCount_0}/{createResource.data[id].Capacity_1}";
        }
        else if(id == 2 && txtAmout.tag == "resource_1")
        {
            txtAmout.text = $"������ 1: {createResource.data[id].ResourceCount_0}/{createResource.data[id].Capacity_0}\n������ 2: {createResource.data[id].ResourceCount_1}/{createResource.data[id].Capacity_1}";
        }
        else if (id == 2 && txtAmout.tag == "resource_2")
        {
            txtAmout.text = $"{createResource.data[id].CurrentAmount}/{createResource.data[id].Capacity_2}";
        }
    }
}
